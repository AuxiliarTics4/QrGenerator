<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('paquetes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('id_equipo');
            $table->string('dispositivo');
            $table->string('referencia');
            $table->string('serial');
            $table->string('procesador');
            $table->string('ram');
            $table->string('discoDuro');
            $table->string('docummento');
            $table->string('nomres_apellidos');
            $table->string('fecha_compra');
            $table->string('proveedor');
            $table->string('estado');
            $table->longText('codigo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('paquetes');
    }
};
