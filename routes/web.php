<?php

use App\Http\Controllers\LoaderController;
use Illuminate\Support\Facades\Route;

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/',[LoaderController::class, 'index']);
Route::get('/ver/{id_equipo}',[LoaderController::class, 'show']);
Route::get('/descargar',[LoaderController::class, 'showAll']);

Route::post('/cargar',[LoaderController::class, 'store']);
Route::post('/ñ',[LoaderController::class, 'FunctionName']);
