<?php

namespace App\Http\Controllers;

use App\Models\equipos;
use App\Models\Paquetes;
use Barryvdh\DomPDF\Facade\Pdf;
use Carbon\Carbon;
use chillerlan\QRCode\QRCode;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\IOFactory;

class LoaderController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $datos = Paquetes::all();

        return view('cargueDatos',compact('datos'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $datos = IOFactory::load($request->file('datos'));

        $hojas = [
                $datos->getSheetByName('PORTATIL'),
                $datos->getSheetByName('TENU-TORRE-SERVIDORES'),
                $datos->getSheetByName('DIADEMA'),
                $datos->getSheetByName('PAD MOUSE'),
                $datos->getSheetByName('MOUSE'),
                $datos->getSheetByName('TECLADO')
        ];

        $filas = array();

        for ($i = 0; $i < count($hojas); $i++) {

            $hoja = $hojas[$i];

            $j = 8;

            while (trim($hoja->getCell('A'.$j)->getValue()) != NULL) {

                $fila = [
                    'id_equipo' => trim($hoja->getCell('A'.$j)->getFormattedValue()),
                    'dispositivo' => trim($hoja->getCell('B'.$j)->getFormattedValue()),
                    'referencia' => trim($hoja->getCell('D'.$j)->getFormattedValue()),
                    'serial' => trim($hoja->getCell('E'.$j)->getFormattedValue()),
                    'procesador' => trim($hoja->getCell('F'.$j)->getFormattedValue()),
                    'ram' => trim($hoja->getCell('G'.$j)->getFormattedValue()),
                    'discoDuro' => trim($hoja->getCell('H'.$j)->getFormattedValue()),
                    'docummento' => trim($hoja->getCell('J'.$j)->getFormattedValue()),
                    'nomres_apellidos' => trim($hoja->getCell('K'.$j)->getFormattedValue()),
                    'fecha_compra' => trim($hoja->getCell('L'.$j)->getFormattedValue()),
                    'proveedor' => trim($hoja->getCell('O'.$j)->getFormattedValue()),
                    'estado' => trim($hoja->getCell('P'.$j)->getFormattedValue()),
                    'codigo' => (new QRCode)->render(url('/ver/'.trim($hoja->getCell('A'.$j)->getFormattedValue()))),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ];

                $j++;
                array_push($filas,$fila);

            }
        }

        Paquetes::insert($filas);

        return redirect()->back();

    }

    /**
     * Display the specified resource.
     */
    public function showAll()
    {
        $datos = Paquetes::where('id_equipo','not like','%SIN CODIGO%')
                        ->where('id_equipo','not like','%SINCODIGO%')
                        ->orderBy('id_equipo','ASC')
                        ->get();
        $pdf = Pdf::loadView('formatos.codigos',compact('datos'));
        $pdf->setPaper('letter','landscape');
        return $pdf->stream('CODIGOS DE EQUIPOS');

    }
    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $datos = Paquetes::where('id_equipo',$id)->first();
        return view('equipo',compact('datos'));
    }

    function FunctionName() {
        $fila = [[
            'id_equipo' => rand(),
            'dispositivo' => rand(),
            'referencia' => rand(),
            'serial' => rand(),
            'procesador' => rand(),
            'ram' => rand(),
            'discoDuro' => rand(),
            'docummento' => rand(),
            'nomres_apellidos' => rand(),
            'fecha_compra' => rand(),
            'proveedor' => rand(),
            'estado' => rand(),
            'codigo' => (new QRCode)->render("AGS SALUD SAS\n900237674-7-MOU-037\n7777777777\n7777777777\nHTTPS://inventario.agssalud.com/900237674-7-MOU-037"),
            'created_at' => now(),
            'updated_at' => now()
        ]];

        Paquetes::insert($fila);

        return redirect('/');
    }
}
