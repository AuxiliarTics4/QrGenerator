@extends('plantilla')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card" style="width: 18rem;">
                <img src="{{ $datos->codigo }}" class="card-img-top" alt="QRCode">
                <div class="card-body">
                    <h5 class="card-title">{{ $datos->id_equipo }}</h5>
                    <p class="card-text">{{ $datos->dispositivo }}</p>
                    <p class="card-text">{{ $datos->referencia }}</p>
                    <p class="card-text">{{ $datos->nomres_apellidos }}</p>
                    <p class="card-text">{{ $datos->docummento }}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
