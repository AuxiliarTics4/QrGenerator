@extends('plantilla')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="d-flex justify-content-between">
                <form action="{{ url('/cargar') }}" enctype="multipart/form-data" method="POST">
                    @csrf
                    <fieldset class="uk-fieldset">
                        <legend class="uk-legend">Legend</legend>
                        <div class="uk-margin" uk-margin>
                            <div uk-form-custom="target: true">
                                <input type="file" name="datos" aria-label="Custom controls">
                                <input class="uk-input uk-form-width-medium" type="text" placeholder="Select file" aria-label="Custom controls" disabled>
                            </div>
                            <button class="uk-button uk-button-default">Submit</button>
                        </div>
                    </fieldset>
                </form>
                <h3><a class="uk-link-reset" href="{{ url('/descargar') }}" target="_blank">DESCARGAR LISTA</a></h3>
            </div>
            <form action="{{ url('/ñ') }}" enctype="multipart/form-data" method="POST">
                @csrf
                <button type="submit">ñ</button>
            </form>
        </div>
    </div>
</div>
@endsection
